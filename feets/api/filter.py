import django_filters

from feets.models import *


class CardFilter(django_filters.FilterSet):
    categories2 = django_filters.ModelChoiceFilter(queryset=Category.objects.all(), field_name='category', method='filter_category', label='block_category')  # Card'ларни блокcategory' бойича филтрлаб чикарябмиз
    categories = django_filters.ModelChoiceFilter(queryset=Category.objects.all(), field_name='category')  # Card'ларни category' бойича филтрлаб чикарябмиз
    tag2 = django_filters.ModelChoiceFilter(queryset=Tags.objects.all(), field_name='tag', method='filter_tag', label='block_tag')  # Card'ларни блокtag' бойича филтрлаб чикарябмиз
    tag = django_filters.ModelChoiceFilter(queryset=Tags.objects.all(), field_name='tag')  # Card'ларни tag' бойича филтрлаб чикарябмиз

    def filter_category(self, queryset, name, value):
        if value:
            return queryset.exclude(category=value)
        return queryset


    def filter_tag(self, queryset, name, value):
        if value:
            return queryset.exclude(tag=value)
        return queryset

    class Meta:
        model = Card
        fields = ['categories', 'categories2', 'tag', 'tag2']



