from rest_framework import viewsets, mixins, filters
from feets.models import *
from feets.api.serializer import *
from Feet_finder.pagination import LargeResultsSetPagination
from feets.api.filter import *
from django_filters.rest_framework import DjangoFilterBackend


class CardViewSet(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):
    queryset = Card.objects.all()
    serializer_class = CardSerializer
    filterset_class = CardFilter

    pagination_class = LargeResultsSetPagination  # paginatsiya

    search_fields = ['name', 'desc']
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]  # поиск API кисмида


class Review_cardViewSet(mixins.CreateModelMixin,
                         mixins.DestroyModelMixin,
                         viewsets.GenericViewSet):
    queryset = Review_card.objects.all()
    serializer_class = Review_cardSerializer


class CategoryViewSet(mixins.ListModelMixin,
                      mixins.RetrieveModelMixin,
                      viewsets.GenericViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    filter_backends = (DjangoFilterBackend,)


class TagsViewSet(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):
    queryset = Tags.objects.all()
    serializer_class = TagsSerializer
    filter_backends = (DjangoFilterBackend,)
