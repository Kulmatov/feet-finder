from feets.models import *
from rest_framework import serializers


class CardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Card
        fields = "__all__"

    def get_category(self, obj):
        return CategorySerializer(obj.category, many=False, context={"request": self.context["request"]}).data

    def get_card(self, obj):
        return CardSerializer(obj.product, many=False, context={"request": self.context["request"]}).data


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"


class Review_cardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review_card
        fields = "__all__"


class TagsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tags
        fields = "__all__"
