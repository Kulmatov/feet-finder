from django.contrib import admin
from feets.models import *

# Register your models here.
admin.site.register(Card)
admin.site.register(Category)
admin.site.register(Tags)
admin.site.register(Review_card)
admin.site.register(BlockTag)
admin.site.register(BlockCategory)
