from django.db import models
from django.contrib.auth.models import AbstractUser


class Card(models.Model):
    name = models.CharField(max_length=200, null=False, blank=True)
    image = models.ImageField(upload_to=" image/", null=True, blank=True)
    desc = models.CharField(max_length=200, null=False, blank=True)
    category = models.ForeignKey('Category', on_delete=models.CASCADE, null=True, blank=True)
    tag = models.ManyToManyField("Tags")

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=100, null=False, blank=True)

    def __str__(self):
        return self.name


class Review_card(models.Model):
    comment = models.CharField(max_length=256)
    product = models.ForeignKey(Card, on_delete=models.CASCADE, null=True)


class Tags(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class BlockTag(models.Model):
    tag = models.ForeignKey(Tags, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.tag


class BlockCategory(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.category
