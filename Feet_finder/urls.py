from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import routers
from feets.api.viewset import *


router = routers.DefaultRouter()
router.register(r'cardList', CardViewSet)
router.register(r'categoryList', CategoryViewSet)
router.register(r'review', Review_cardViewSet)


urlpatterns = [
                  path('api/v1/', include(router.urls)),
                  path('admin/', admin.site.urls),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
              + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
